import { useEffect, useState } from 'react';
import './App.css';
import ModalImage from './components/ModalImag';
import ModalText from './components/ModalText';
import Button from './components/Button/Button';
import TextElement from './components/TextElement/TextElement';
import ProductList from './components/ProductList/ProductList';
import Header from './components/header/Header.jsx';





const App = () => {
  const initialFavorite = localStorage.getItem("fav")?JSON.parse(localStorage.getItem("fav")):[];
  const initialBasket = localStorage.getItem("basket")?JSON.parse(localStorage.getItem("basket")):[];
  const [products, setProducts] =useState([]);
  const [favorite, setFavorite] =useState(initialFavorite);
  const [basket, setBasket] =useState(initialBasket);
  const [currentArtikul, setCurrentArtikul ] = useState(null);
  const isBasket = basket.includes(currentArtikul); 
  const addToBasket = (articul) => {
    setBasket((prev) => {
      const isBasket = prev.includes(articul) 
      if (!isBasket) {
        return [...prev,articul]
      } else {
        return prev
      }
    })

  }
  const togalFavorite = (articul) => {
    setFavorite((prev) => {console.log(prev)
      const isFavorite = prev.includes(articul)
    if (isFavorite) { return prev.filter((favId) => {
     if (favId===articul) {
     return false
     } else {
      return true 
     }
    })
    }else { 
     return [...prev,articul]
    }
    })
    
  }

  // const [ showModal2, setShowModal2 ] = useState(false);

  const setArtikul = (artikul) => {
    setCurrentArtikul(artikul);
  };

  // const openModal2 = () => {
  //   setShowModal2(true);
  // };

  const closeModal = () => {
    setCurrentArtikul(null);
  };

  // const closeModal2 = () => {
  //   setShowModal2(false);
  // };
  useEffect(() => {
    fetch("./product.json")
    .then((response) => 
     response.json()
    ).then((data) => {
      setProducts(data)
    })
  }, []);
  useEffect(()=> {
    localStorage.setItem("fav", JSON.stringify(favorite))
    localStorage.setItem("basket", JSON.stringify(basket))

  },[favorite,basket])
  return (
    
    
    <div className="App">
      <Header favorite={favorite} basket={basket}/>
      <ProductList products={products} togalFavorite={togalFavorite} favorite={favorite} setArtikul={setArtikul}/>
     
      {currentArtikul &&!isBasket&& (
        <ModalImage
        
        closeButton={true}
        text={<>
        <TextElement type='h2' marginB='32px'>Product Add!</TextElement>
        <TextElement marginB='64px'>By clicking the “Yes, Add” button, PRODUCT NAME will be added.</TextElement>
        </>}
        closeModal={closeModal}
        footerProps={{firstText:"NO, CANCEL", secondaryText:"YES, Add To Basket", firstClick:closeModal, secondaryClick:()=> {addToBasket(currentArtikul);closeModal()}, firstClass:'cancel', firstBackground:'#8A33FD'}}
        imageSrs={'./image.png'}
       
       />
       
      )}
      {isBasket && (
   <ModalText
  
  closeButton={true}
  text={<>
     
   <TextElement marginB='73px'>Product Added To Basket</TextElement>
   </>}
   closeModal={closeModal}
   footerProps={{firstText:"OK, CLOSE", firstClick:closeModal, firstBackground:'#8A33FD', firstClass:'cancelWithMargin'}}
  />
 )}



    </div>
  );
};


export default App;
