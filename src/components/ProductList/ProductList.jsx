import Product from "../Product/Products"
import PropTypes from 'prop-types';
import styles from "./ProductList.module.scss"

const ProductList = ({products, togalFavorite, favorite, setArtikul}) => {
   
    return (
        <ul className={styles.list}>
            {products.map((product) =>{
        
        return (
          <Product product={product} key={product.articul} togalFavorite={togalFavorite} isFavorite={favorite.includes(product.articul)} setArtikul={setArtikul} />
        )

      })
      }
        </ul>
        
    )
}
ProductList.propTypes = {
  products: PropTypes.array,
  togalFavorite: PropTypes.func,
  favorite: PropTypes.array,
  setArtikul: PropTypes.func,
};
export default ProductList