import React from 'react';
import PropTypes from 'prop-types';
import styles from './Button.module.css';

const Button = ({ backgroundColor, text, onClick, className}) => {
    return (
        <button className= {`${styles.button} ${styles[className]}`}
        style={{ backgroundColor: backgroundColor }}
        onClick={onClick}
        >
            {text}
        </button>
    );
};

Button.propTypes = {
    backgroundColor: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
};

export default Button;