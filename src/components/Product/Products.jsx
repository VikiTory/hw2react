import Button from "../Button/Button"
import styles from "./Product.module.scss"
import {ReactComponent as FavoriteIcon} from "../../assets/favorite.svg"
import PropTypes from 'prop-types';
const Product = ({product, togalFavorite, isFavorite, setArtikul}) => {
    const color = isFavorite?"gold":"white" 
    return (
        <li className={styles.item} ><h3>{product.name}</h3>
        <FavoriteIcon onClick={() => {togalFavorite(product.articul)}} className={styles.svg} style={{fill:color}}/>
          <img className={styles.img} src={product.url} alt={product.name}/>
          <div>
          <h3>{product.price}</h3>
          <h3>{product.articul}</h3>
          <h3>{product.color}</h3>
          <Button
      backgroundColor="blue"
      text="Add to card"
      onClick={() => {
      setArtikul(product.articul)
      }} />
      </div>
      </li>
    )
}
Product.propTypes = {
  product: PropTypes.object,
  togalFavorite: PropTypes.func,
  isFavorite: PropTypes.bool,
  setArtikul: PropTypes.func,
};
export default Product