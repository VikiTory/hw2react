import styles from './TextElement.module.css'
import PropTypes from 'prop-types';
const TextElement = ({type, marginB, children})=> {
    return (
        <p className={styles[type]} style= {{margin:0, marginBottom:marginB}}>{children}</p>

    )
}
TextElement.propTypes = {
    type: PropTypes.string,
    marginB: PropTypes.string,
    children: PropTypes.node.isRequired,
  };
  TextElement.defaultProps = {
    type: 'p',
  };
export default TextElement