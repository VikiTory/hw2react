
import Modal from "./modal/Modal";
import ModalBody from "./modal/ModalBody";
import ModalClose from "./modal/ModalClose";
import ModalFooter from "./modal/ModalFooter";
import ModalHeader from "./modal/ModalHeader";
import ModalWrapper from "./modal/ModalWrapper";
import PropTypes from 'prop-types';

const ModalImage = ({closeButton, text, closeModal, footerProps, imageSrs}) => {
    console.log(closeModal)
  return (
    <ModalWrapper click={(e) => {
        e.stopPropagation()
        closeModal()
    }} >
      <Modal>
        <ModalHeader>
        {closeButton && <ModalClose click={closeModal}/>}
        </ModalHeader>
        <ModalBody><><img style={{marginBottom:"85px"}} src={imageSrs} alt="img" />{text}</></ModalBody>
        <ModalFooter {...footerProps}></ModalFooter>
      </Modal>
    </ModalWrapper>
  );
};
ModalImage.propTypes = {
  closeButton: PropTypes.bool,
  text: PropTypes.string,
  closeModal: PropTypes.func,
  footerProps: PropTypes.object,
  imageSrs: PropTypes.string,
};
export default ModalImage
