const Search = () => {
    return (
        <SearchForm nameForm="form">
            <SearchButton
            type="submit"
            className="form_button"
            click={preventDefault}
            />
            <SearchInput type="text" className="form_input"/>
        </SearchForm>
    );
};
Search.propTypes = {
    nameForm:PropTypes.string,
    type: PropTypes.string,
};
export default Search;