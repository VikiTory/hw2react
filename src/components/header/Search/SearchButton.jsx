const SearchButton = ({ className, click, type}) => {
    return (
        <button type={type} className={className} onClick={click}>
            <Loop/>
        </button>
    );
};
SearchButton.propTypes = {
    className: PropTypes.string,
    type: PropTypes.string,
};
export default SearchButton;