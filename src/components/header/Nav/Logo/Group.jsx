const Group = ({ text }) => {
    return <span className="logo_group">{text}</span>;
};
Group.defaultProps = {
    text: "Keep is classy",
};
Group.propTypes = {
    text: PropTypes.string,
    className: PropTypes.string,
};
export default Group;