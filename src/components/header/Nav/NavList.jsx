const NavList = ({ children }) => {
    return <ul className="nav_list">{children}</ul>;
};
NavList.propTypes = {
    children: PropTypes.any,
    className: PropTypes.string,
};
export default NavList;