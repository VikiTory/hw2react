const ListItem = ({ text, classNames }) => {
    return <li classname={cx("nav__item", classNames)}>{text}</li>;
};
ListItem.propTypes = {
    text: PropTypes.string,
    className: PropTypes.string,
    classNames: PropTypes.string,
};
export default ListItem;