const Nav = () => {
    return (
        <div className="nav">
            <Logo/>
            <NavList>
                <ListItem classNames="nav__item-bold" text="Shop" />
                <ListItem text="Men" />
                <ListItem text="Woman" />
                <ListItem text="Combos" />
                <ListItem text="Joggers" />
            </NavList>
        </div>
    );
};
Nav.propTypes = {
    text: PropTypes.string,
    classNames: PropTypes.string,
};
export default Nav;