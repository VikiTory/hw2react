import {ReactComponent as FavoriteIcon} from "../../assets/favorite.svg"
import {ReactComponent as BasketIcon} from "../../assets/basket.svg"
import PropTypes from 'prop-types';
import styles from "./Header.module.scss"
const Header = ({favorite, basket})=>{
    return (
        <header className={styles.header}><div className={`${styles.wrapper} container`}><h2><FavoriteIcon/>{favorite.length}</h2><h2><BasketIcon/>{basket.length}</h2></div></header>
        

    )
}
Header.propTypes = {
    favorite: PropTypes.array,
    basket: PropTypes.array,
    
  };
export default Header