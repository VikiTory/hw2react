const ModalWrapper = ({children, click}) => {
    return <div className='modal' onClick={click}>{children}</div>

}

export default ModalWrapper